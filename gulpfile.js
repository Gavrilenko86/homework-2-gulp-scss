const {series, parallel, src, dest, watch} = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

//Очищення папки dist
function cleanDist() {
    return src('./dist/', {read: false, allowEmpty: true})
        .pipe(clean())
}

function cleanSCSS() {
    return src('./dist/scss', {read: false, allowEmpty: true})
        .pipe(clean())
}

//Копіювання файлів з робочої папки в dist
function copy() {
    return src('./src/**/*.*')
        .pipe(dest('./dist'))
}


// Компіляція, мініфікація, очищення непотрібного коду
function compileCSS() {
    return src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(concat("styles.min.css"))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 3 versions'],
            grid: true,
            cascade: true
        }))
        .pipe(dest('./dist/css'))
}

// Мінімізація картинок та копіювання в dist
function imageMin() {
    return src('./src/img/**/*.*')
        .pipe(imagemin())
        .pipe(dest('./dist/img'))
}

function js(){
    return src('./src/js/*.js')
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(dest('./dist/js'))
}

// Запуск сервера
function startServer() {
    browserSync.init( {
        server: {
            baseDir: './'
        }
    });
    watch('./src/scss/*.scss', compileCSS).on('change', browserSync.reload);
    watch('./src/img/**/*.*', imageMin);
    watch('./src/js/**/*.js', js);
    watch('index.html').on('change', browserSync.reload);
}


exports.cleanDist = cleanDist;
exports.cleanSCSS = cleanSCSS;
exports.copy = copy;
exports.compileCSS = compileCSS;
exports.imageMin = imageMin;
exports.js = js;


exports.dev = series(cleanDist, compileCSS, js, copy, cleanSCSS, startServer);
exports.build = series(cleanDist, compileCSS, js, imageMin, copy, cleanSCSS);
